@isTest
public class mapPractices_Test {
     static User usr{get; set;}
     @TestSetup
    static void inserRecordsInDB(){
        usr = [SELECT ID FROM User WHERE LastName = 'Mukha'];
        List<Account> accs = new List<Account>();
        for (Integer i = 0; i < 5; i++) {
            accs.add(new Account(Name = 'TestAccount'+i));
        }
        insert accs;
    }
    static TestMethod void getMapAccounts_Test() {
       Map<String, String> result = mapPractices.getMapAccounts();
       Boolean isWork = false;
       System.debug(result);
       System.debug(result.size());
       if (result.size()>1) isWork = true;
       System.assertEquals(true, isWork, 'getMapAccounts: wrong result (False)!');
    }
    @IsTest
    static void convertListToMap_Test(){
        Test.startTest();
       
            List<Account> listAccount = [SELECT ID,Name FROM Account];
            Map<ID,sObject> listToMapAccount =  mapPractices.convertListToMap(listAccount);
           // System.asse
           Boolean isWork = false;
            if (listToMapAccount.size()!=0) {
                isWork=true;
            }
            System.assertEquals(true,isWork,'convertListToMap: Wrong result (False)!');
        Test.stopTest();  
    }
}
