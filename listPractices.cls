public class listPractices {
    public static List<Object> getLastListItem(List<Object> inputeList, Integer mode) {
        Integer inputeListSize =  inputeList.size();
        Object lastElement = inputeList[inputeListSize-1];
        if (mode ==1) {
            System.debug(lastElement);
            System.debug('List after in 1st mode: '+inputeList);
        }
        if (mode == 2) {
            System.debug(inputeList.remove(inputeListSize-1));
            
            System.debug('List after in 2nd mode: '+inputeList);
        }   
        return inputeList;   
    }

    public static List<Object> addElementToEnd(List<Object> inputeList, Object element, Integer mode) {
        switch on mode {
            when  1 {
                System.debug('Put on the existing list: '+inputeList);
                inputeList.add(element);
                System.debug('Elements after adding: '+inputeList);
                return inputeList;
            }
            when 2 {
                System.debug('Put in to new list:');
                List<Object> newList = new List<Object>();
                newList.add(element);
                System.debug('New list: '+newList);

                return newList;
            }
            when else {return null;}
        }
        
    }

    public static void printListItems(List<Object> listToView) {
        for (Object element : listToView) {
            System.debug(element);
        }
    }

    public static List<Integer> createList(Integer firstNum, Integer secondNum) {
        Integer min,max;
        if(firstNum>secondNum){
            min = secondNum;
            max = firstNum;
        } else {
            min = firstNum;
            max = secondNum;
        }
        List<Integer> listIntElements = new List<Integer>();
        for (Integer i = min; i <= max; i++) {
            listIntElements.add(i);
        }
        listIntElements.sort();
        return listIntElements;
    }
   public static Integer countListItems(List<Integer> inputeList, integer index) {
        if (inputeList.size()>=index) {
            Integer sum=0;
            for (Integer i = index-1; i < inputeList.size(); i++) {
                sum+=inputeList[i];
            }
            return sum;
        } else return 0;
        
   }
   public static List<Integer> bubbleSort(List<Integer> inputeList){
       Boolean isSorted = false; 
       Integer box;
       while (!isSorted) {
            isSorted=true;
           for (Integer i = 0; i < inputeList.size()-1; i++){
               if(inputeList[i]>inputeList[i+1]) {
                   
                isSorted= false;
                box = inputeList[i+1];
                inputeList[i+1] = inputeList[i];
                inputeList[i] = box;
               }
           }
       }
       return inputeList;
   }

   public static Integer binarySearch(List<Integer> inputeList, Integer elementToSearch) {
        Integer right = inputeList[inputeList.size()-1];
        Integer left = inputeList[0];
        Integer mid = inputeList[inputeList.size()/2];

        //System.debug('left '+ left+ ' mid '+ mid + ' right '+right);
        
        Integer frm, to;
        Integer result;

        if (elementToSearch>mid) {
            frm = inputeList.size()/2;
            to = inputeList.size()-1;
        } else {frm = 0; to=inputeList.size()/2;}
        System.debug(frm+':'+to);
        for (Integer i = frm; i <= to; i++) {
              //  System.debug(inputeList[i]+'='+elementToSearch+'?');
            if (inputeList[i]==elementToSearch) {
                result = i;
                break;
            } else result = -1;
        }
        return result;
   }

   public static Boolean isSymmetricMatrix(List<List<Integer>> matrix) {
       Boolean isMatch = true;
       for (Integer i = 0; i < matrix.size(); i++) {
           for (Integer j = 0; j < matrix.size(); j++) {
               if(matrix[i][j]!=matrix[j][i]){ isMatch = false; break;}
           }
       }
    return isMatch;
   }

}
